import requests
from django.shortcuts import render, redirect, HttpResponse
from datetime import datetime
from django.contrib.auth import logout
from django.contrib import messages
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.conf import settings 


@login_required(login_url=settings.LOGIN_URL)
def home(request):
    template = 'home.html'
    return render(request, template)

@login_required(login_url=settings.LOGIN_URL)
def logout_custom(request):
    logout(request)
    return redirect('masuk')

#@login_required(login_url=settings.LOGIN_URL)
def signup(request):
    if request.POST:
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, "User berhasil dibuat!")
            return redirect('signup')
        else:
            messages.error(request, "Terjadi kesalahan!")
            return redirect('signup')
    else:
        form = UserCreationForm()
        konteks = {
            'form':form,
        }
    return render(request, 'signup.html', konteks)

@login_required(login_url=settings.LOGIN_URL)
def users(request):
    users = User.objects.all()
    template = 'users.html'
    context = {
        'users':users,
    }
    return render(request, template, context)

@login_required(login_url=settings.LOGIN_URL)
def get_pipeline_data(request):
    access_token = 'glpat-QhAKD253UX8cc9CLJKMs'  
    api_url = 'https://gitlab.com/api/v4/projects/56091294/jobs'
    headers = {'PRIVATE-TOKEN': access_token}
    try:
        response = requests.get(api_url, headers=headers)
        data = response.json()
        context = {'pipeline_data': data}
        template = 'pipeline.html'
        return render(request, template, context)
    except Exception as e:
        error_message = str(e)
        context = {'error_message': error_message}
        template = 'pipeline.html'
        return render(request, template, context)

    
# api_url = 'https://gitlab.com/api/v4/projects/56091294/pipelines/1223616642/'