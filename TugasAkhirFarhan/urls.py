from django.contrib import admin
from django.urls import path
from web.views import *
from django.contrib.auth.views import LoginView, LogoutView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', home, name='home'),  # Menghapus tanda '/' di awal
    path('masuk/', LoginView.as_view(), name='masuk'),
    path('keluar/', logout_custom, name='keluar'),
    path('user/', users, name='users'),
    path('user/add/', signup, name='signup'),
    path('pipeline/', get_pipeline_data, name='pipeline'),
]
